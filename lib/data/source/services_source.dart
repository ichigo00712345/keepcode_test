import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '../../constants/api_constants.dart';

part 'services_source.g.dart';

@RestApi(baseUrl: ApiCustomer.host)
abstract class ServicesSource {
  factory ServicesSource(Dio dio, {String? baseUrl}) = _ServicesSource;

  @GET(ApiCustomer.allServices)
  Future<String> fetchServices(@Queries() Map<String, dynamic> queries);
  @GET(ApiCustomer.buyNumber)
  Future<String> buyNumber(@Queries() Map<String, dynamic> queries);
}

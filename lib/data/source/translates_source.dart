import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '../../constants/api_constants.dart';

part 'translates_source.g.dart';

@RestApi(baseUrl: ApiCustomer.host)
abstract class TranslatesSource {
  factory TranslatesSource(Dio dio, {String? baseUrl}) = _TranslatesSource;

  @GET(ApiCustomer.translates)
  Future<String> fetchTranslates(@Queries() Map<String, dynamic> queries);
}

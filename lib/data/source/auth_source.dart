import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '../../constants/api_constants.dart';

part 'auth_source.g.dart';

@RestApi(baseUrl: ApiCustomer.host)
abstract class AuthSource {
  factory AuthSource(Dio dio, {String? baseUrl}) = _AuthSource;

  @GET(ApiCustomer.auth)
  Future<String> authorization(@Queries() Map<String, dynamic> queries);
}

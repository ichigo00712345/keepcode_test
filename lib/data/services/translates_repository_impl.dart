import 'package:dartz/dartz.dart';
import 'package:keepcode_test/data/source/translates_source.dart';
import 'package:keepcode_test/domain/repositories/translates_repository.dart';

import '../../common/failure.dart';
import '../../common/request_wrapper.dart';

class TranslatesRepositoryImpl implements TranslatesRepository {
  final TranslatesSource source;
  TranslatesRepositoryImpl({required this.source});
  @override
  Future<Either<Failure, dynamic>> fetchTranslates() async {
    final Map<String, dynamic> queries = {
      "owner": "6",
      "action": "getAllServicesAndAllCountries",
    };
    final wrapper = RequestWrapper<dynamic>();
    return wrapper.request(
      () async {
        return await source.fetchTranslates(queries);
      },
    );
  }
}

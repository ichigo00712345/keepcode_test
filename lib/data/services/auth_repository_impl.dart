import 'package:dartz/dartz.dart';

import '../../common/failure.dart';
import '../../common/request_wrapper.dart';
import '../../domain/repositories/auth_repository.dart';
import '../source/auth_source.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthSource source;
  AuthRepositoryImpl({required this.source});
  @override
  Future<Either<Failure, String>> authorization() async {
    final Map<String, dynamic> queries = {
      "owner": "6",
      "email": "adwtrafanet@yandex.ru",
      "code": "111111",
      "action": "confirmEmailByCode",
    };
    final wrapper = RequestWrapper<String>();
    return wrapper.request(
      () async {
        return await source.authorization(queries);
      },
    );
  }
}

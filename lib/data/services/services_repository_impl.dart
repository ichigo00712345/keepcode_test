// ServicesRepository

import 'package:dartz/dartz.dart';
import 'package:keepcode_test/data/source/services_source.dart';

import '../../common/failure.dart';
import '../../common/request_wrapper.dart';
import '../../domain/repositories/services_repository.dart';

class ServicesRepositoryImpl implements ServicesRepository {
  final ServicesSource source;
  ServicesRepositoryImpl({required this.source});
  @override
  Future<Either<Failure, String>> fetchServices() async {
    final Map<String, dynamic> queries = {
      "owner": "6",
      "action": "getAllServices",
    };
    final wrapper = RequestWrapper<String>();
    return wrapper.request(
      () async {
        return await source.fetchServices(queries);
      },
    );
  }

  @override
  Future<Either<Failure, String>> buyNumber({required String serviceId}) async {
    final Map<String, dynamic> queries = {
      "refresh_token": "57732d7a05fb0fda93e4783c97788a21",
      "sessionId": "5210a808d88da491087fcb9ec792291c",
      "owner": "6",
      "userid": "1155497",
      "country": "0",
      "service": serviceId,
      "action": "getNumberMobile",
      "forward": "0",
      "operator": "any",
    };
    final wrapper = RequestWrapper<String>();
    return wrapper.request(
      () async {
        return await source.fetchServices(queries);
      },
    );
  }
}

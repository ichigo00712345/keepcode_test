import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './get_it.dart' as get_it;
import 'bloc_observer.dart';
import 'view/pages/my_app.dart';

void main() async {
  // WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  await get_it.init();
  Bloc.observer = MyBlocObserver();

  runApp(const MyApp());
  // await Future.delayed(const Duration(seconds: 2));
  // FlutterNativeSplash.remove();
}

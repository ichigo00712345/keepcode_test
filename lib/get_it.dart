import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'constants/hive_constants.dart';
import 'data/services/auth_repository_impl.dart';
import 'data/services/services_repository_impl.dart';
import 'data/services/translates_repository_impl.dart';
import 'data/source/auth_source.dart';
import 'data/source/services_source.dart';
import 'data/source/translates_source.dart';
import 'domain/models/auth_data_model.dart';
import 'domain/models/translates_data_model.dart';
import 'domain/repositories/auth_repository.dart';
import 'domain/repositories/services_repository.dart';
import 'domain/repositories/translates_repository.dart';
import 'view/bloc/auth_bloc/auth_bloc.dart';
import 'view/bloc/services_bloc/services_bloc.dart';
import 'view/bloc/translates_bloc/translates_bloc.dart';

final sl = GetIt.instance;

init() async {
  //auth
  sl.registerFactory(() => AuthBloc(sl()));
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImpl(source: sl()));
  sl.registerLazySingleton<AuthSource>(() => AuthSource(sl()));
  //translates
  sl.registerFactory(() => TranslatesBloc(sl()));
  sl.registerLazySingleton<TranslatesRepository>(() => TranslatesRepositoryImpl(source: sl()));
  sl.registerLazySingleton<TranslatesSource>(() => TranslatesSource(sl()));
  //services
  sl.registerFactory(() => ServicesBloc(sl()));
  sl.registerLazySingleton<ServicesRepository>(() => ServicesRepositoryImpl(source: sl()));
  sl.registerLazySingleton<ServicesSource>(() => ServicesSource(sl()));
  //dio
  sl.registerLazySingleton(() => Dio());
  //hive
  await Hive.initFlutter();
  Hive.registerAdapter(AuthDataModelAdapter());
  Hive.registerAdapter(ServicesTranslatesListAdapter());
  Hive.registerAdapter(ServicesTranslatesItemAdapter());
  final Box<dynamic> hive = await Hive.openBox(HiveConstants.boxName);
  sl.registerLazySingleton(() => hive);
}

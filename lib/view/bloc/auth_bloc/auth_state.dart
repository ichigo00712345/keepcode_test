part of 'auth_bloc.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState.loading() = Loading;
  const factory AuthState.alreadyLogged() = AlreadyLogged;
  const factory AuthState.needAuthorize() = NeedAuthorize;
  const factory AuthState.authorizationSuccess() = AuthorizationSuccess;
  const factory AuthState.authorizationError() = AuthorizationError;
}

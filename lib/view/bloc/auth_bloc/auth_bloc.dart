import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:keepcode_test/domain/repositories/auth_repository.dart';

import '../../../constants/app_constants.dart';
import '../../../domain/models/auth_data_model.dart';
import '../../../get_it.dart' as get_it;

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository repository;
  AuthBloc(this.repository) : super(const Loading()) {
    final hiveBox = get_it.sl<Box>();
    on<CheckAuthorizationStatus>((event, emit) async {
      emit(const Loading());
      await Future.delayed(const Duration(seconds: 1));
      // emit(const NeedAuthorize());
      if (hiveBox.containsKey(AppConstants.authTime)) {
        final DateTime nowTime = DateTime.now();
        final int tokenExpiredEpochTimeInMilliseconds = hiveBox.get(AppConstants.authTime);
        final DateTime tokenExpiredTime = DateTime.fromMillisecondsSinceEpoch(tokenExpiredEpochTimeInMilliseconds);
        if (tokenExpiredTime.isAfter(nowTime)) {
          emit(const AlreadyLogged());
        } else {
          emit(const NeedAuthorize());
        }
      } else {
        emit(const NeedAuthorize());
      }
    });
    on<Authorization>(
      (event, emit) async {
        emit(const Loading());
        final result = await repository.authorization();
        result.fold(
          (l) {
            emit(const AuthorizationError());
          },
          (r) {
            final authInfo = authDataModelFromJson(r);
            final int authorizationTime = DateTime.now().add(const Duration(hours: 1)).millisecondsSinceEpoch;
            hiveBox.put(AppConstants.authTime, authorizationTime);
            hiveBox.put(AppConstants.authInfo, authInfo);
            emit(const AuthorizationSuccess());
          },
        );
      },
    );
  }
}

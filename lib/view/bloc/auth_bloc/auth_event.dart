part of 'auth_bloc.dart';

@freezed
class AuthEvent with _$AuthEvent {
  const factory AuthEvent.checkAuthorizationStatus() = CheckAuthorizationStatus;
  const factory AuthEvent.authorization() = Authorization;
}

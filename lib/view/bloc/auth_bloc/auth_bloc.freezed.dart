// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() checkAuthorizationStatus,
    required TResult Function() authorization,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? checkAuthorizationStatus,
    TResult? Function()? authorization,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? checkAuthorizationStatus,
    TResult Function()? authorization,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CheckAuthorizationStatus value)
        checkAuthorizationStatus,
    required TResult Function(Authorization value) authorization,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CheckAuthorizationStatus value)? checkAuthorizationStatus,
    TResult? Function(Authorization value)? authorization,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CheckAuthorizationStatus value)? checkAuthorizationStatus,
    TResult Function(Authorization value)? authorization,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res, AuthEvent>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res, $Val extends AuthEvent>
    implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CheckAuthorizationStatusCopyWith<$Res> {
  factory _$$CheckAuthorizationStatusCopyWith(_$CheckAuthorizationStatus value,
          $Res Function(_$CheckAuthorizationStatus) then) =
      __$$CheckAuthorizationStatusCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckAuthorizationStatusCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$CheckAuthorizationStatus>
    implements _$$CheckAuthorizationStatusCopyWith<$Res> {
  __$$CheckAuthorizationStatusCopyWithImpl(_$CheckAuthorizationStatus _value,
      $Res Function(_$CheckAuthorizationStatus) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckAuthorizationStatus implements CheckAuthorizationStatus {
  const _$CheckAuthorizationStatus();

  @override
  String toString() {
    return 'AuthEvent.checkAuthorizationStatus()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckAuthorizationStatus);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() checkAuthorizationStatus,
    required TResult Function() authorization,
  }) {
    return checkAuthorizationStatus();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? checkAuthorizationStatus,
    TResult? Function()? authorization,
  }) {
    return checkAuthorizationStatus?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? checkAuthorizationStatus,
    TResult Function()? authorization,
    required TResult orElse(),
  }) {
    if (checkAuthorizationStatus != null) {
      return checkAuthorizationStatus();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CheckAuthorizationStatus value)
        checkAuthorizationStatus,
    required TResult Function(Authorization value) authorization,
  }) {
    return checkAuthorizationStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CheckAuthorizationStatus value)? checkAuthorizationStatus,
    TResult? Function(Authorization value)? authorization,
  }) {
    return checkAuthorizationStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CheckAuthorizationStatus value)? checkAuthorizationStatus,
    TResult Function(Authorization value)? authorization,
    required TResult orElse(),
  }) {
    if (checkAuthorizationStatus != null) {
      return checkAuthorizationStatus(this);
    }
    return orElse();
  }
}

abstract class CheckAuthorizationStatus implements AuthEvent {
  const factory CheckAuthorizationStatus() = _$CheckAuthorizationStatus;
}

/// @nodoc
abstract class _$$AuthorizationCopyWith<$Res> {
  factory _$$AuthorizationCopyWith(
          _$Authorization value, $Res Function(_$Authorization) then) =
      __$$AuthorizationCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthorizationCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$Authorization>
    implements _$$AuthorizationCopyWith<$Res> {
  __$$AuthorizationCopyWithImpl(
      _$Authorization _value, $Res Function(_$Authorization) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Authorization implements Authorization {
  const _$Authorization();

  @override
  String toString() {
    return 'AuthEvent.authorization()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Authorization);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() checkAuthorizationStatus,
    required TResult Function() authorization,
  }) {
    return authorization();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? checkAuthorizationStatus,
    TResult? Function()? authorization,
  }) {
    return authorization?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? checkAuthorizationStatus,
    TResult Function()? authorization,
    required TResult orElse(),
  }) {
    if (authorization != null) {
      return authorization();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CheckAuthorizationStatus value)
        checkAuthorizationStatus,
    required TResult Function(Authorization value) authorization,
  }) {
    return authorization(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CheckAuthorizationStatus value)? checkAuthorizationStatus,
    TResult? Function(Authorization value)? authorization,
  }) {
    return authorization?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CheckAuthorizationStatus value)? checkAuthorizationStatus,
    TResult Function(Authorization value)? authorization,
    required TResult orElse(),
  }) {
    if (authorization != null) {
      return authorization(this);
    }
    return orElse();
  }
}

abstract class Authorization implements AuthEvent {
  const factory Authorization() = _$Authorization;
}

/// @nodoc
mixin _$AuthState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() alreadyLogged,
    required TResult Function() needAuthorize,
    required TResult Function() authorizationSuccess,
    required TResult Function() authorizationError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? alreadyLogged,
    TResult? Function()? needAuthorize,
    TResult? Function()? authorizationSuccess,
    TResult? Function()? authorizationError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? alreadyLogged,
    TResult Function()? needAuthorize,
    TResult Function()? authorizationSuccess,
    TResult Function()? authorizationError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(AlreadyLogged value) alreadyLogged,
    required TResult Function(NeedAuthorize value) needAuthorize,
    required TResult Function(AuthorizationSuccess value) authorizationSuccess,
    required TResult Function(AuthorizationError value) authorizationError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(AlreadyLogged value)? alreadyLogged,
    TResult? Function(NeedAuthorize value)? needAuthorize,
    TResult? Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult? Function(AuthorizationError value)? authorizationError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(AlreadyLogged value)? alreadyLogged,
    TResult Function(NeedAuthorize value)? needAuthorize,
    TResult Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult Function(AuthorizationError value)? authorizationError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res, AuthState>;
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res, $Val extends AuthState>
    implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadingCopyWith<$Res> {
  factory _$$LoadingCopyWith(_$Loading value, $Res Function(_$Loading) then) =
      __$$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res, _$Loading>
    implements _$$LoadingCopyWith<$Res> {
  __$$LoadingCopyWithImpl(_$Loading _value, $Res Function(_$Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Loading implements Loading {
  const _$Loading();

  @override
  String toString() {
    return 'AuthState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() alreadyLogged,
    required TResult Function() needAuthorize,
    required TResult Function() authorizationSuccess,
    required TResult Function() authorizationError,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? alreadyLogged,
    TResult? Function()? needAuthorize,
    TResult? Function()? authorizationSuccess,
    TResult? Function()? authorizationError,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? alreadyLogged,
    TResult Function()? needAuthorize,
    TResult Function()? authorizationSuccess,
    TResult Function()? authorizationError,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(AlreadyLogged value) alreadyLogged,
    required TResult Function(NeedAuthorize value) needAuthorize,
    required TResult Function(AuthorizationSuccess value) authorizationSuccess,
    required TResult Function(AuthorizationError value) authorizationError,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(AlreadyLogged value)? alreadyLogged,
    TResult? Function(NeedAuthorize value)? needAuthorize,
    TResult? Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult? Function(AuthorizationError value)? authorizationError,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(AlreadyLogged value)? alreadyLogged,
    TResult Function(NeedAuthorize value)? needAuthorize,
    TResult Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult Function(AuthorizationError value)? authorizationError,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading implements AuthState {
  const factory Loading() = _$Loading;
}

/// @nodoc
abstract class _$$AlreadyLoggedCopyWith<$Res> {
  factory _$$AlreadyLoggedCopyWith(
          _$AlreadyLogged value, $Res Function(_$AlreadyLogged) then) =
      __$$AlreadyLoggedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AlreadyLoggedCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res, _$AlreadyLogged>
    implements _$$AlreadyLoggedCopyWith<$Res> {
  __$$AlreadyLoggedCopyWithImpl(
      _$AlreadyLogged _value, $Res Function(_$AlreadyLogged) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AlreadyLogged implements AlreadyLogged {
  const _$AlreadyLogged();

  @override
  String toString() {
    return 'AuthState.alreadyLogged()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AlreadyLogged);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() alreadyLogged,
    required TResult Function() needAuthorize,
    required TResult Function() authorizationSuccess,
    required TResult Function() authorizationError,
  }) {
    return alreadyLogged();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? alreadyLogged,
    TResult? Function()? needAuthorize,
    TResult? Function()? authorizationSuccess,
    TResult? Function()? authorizationError,
  }) {
    return alreadyLogged?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? alreadyLogged,
    TResult Function()? needAuthorize,
    TResult Function()? authorizationSuccess,
    TResult Function()? authorizationError,
    required TResult orElse(),
  }) {
    if (alreadyLogged != null) {
      return alreadyLogged();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(AlreadyLogged value) alreadyLogged,
    required TResult Function(NeedAuthorize value) needAuthorize,
    required TResult Function(AuthorizationSuccess value) authorizationSuccess,
    required TResult Function(AuthorizationError value) authorizationError,
  }) {
    return alreadyLogged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(AlreadyLogged value)? alreadyLogged,
    TResult? Function(NeedAuthorize value)? needAuthorize,
    TResult? Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult? Function(AuthorizationError value)? authorizationError,
  }) {
    return alreadyLogged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(AlreadyLogged value)? alreadyLogged,
    TResult Function(NeedAuthorize value)? needAuthorize,
    TResult Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult Function(AuthorizationError value)? authorizationError,
    required TResult orElse(),
  }) {
    if (alreadyLogged != null) {
      return alreadyLogged(this);
    }
    return orElse();
  }
}

abstract class AlreadyLogged implements AuthState {
  const factory AlreadyLogged() = _$AlreadyLogged;
}

/// @nodoc
abstract class _$$NeedAuthorizeCopyWith<$Res> {
  factory _$$NeedAuthorizeCopyWith(
          _$NeedAuthorize value, $Res Function(_$NeedAuthorize) then) =
      __$$NeedAuthorizeCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NeedAuthorizeCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res, _$NeedAuthorize>
    implements _$$NeedAuthorizeCopyWith<$Res> {
  __$$NeedAuthorizeCopyWithImpl(
      _$NeedAuthorize _value, $Res Function(_$NeedAuthorize) _then)
      : super(_value, _then);
}

/// @nodoc

class _$NeedAuthorize implements NeedAuthorize {
  const _$NeedAuthorize();

  @override
  String toString() {
    return 'AuthState.needAuthorize()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NeedAuthorize);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() alreadyLogged,
    required TResult Function() needAuthorize,
    required TResult Function() authorizationSuccess,
    required TResult Function() authorizationError,
  }) {
    return needAuthorize();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? alreadyLogged,
    TResult? Function()? needAuthorize,
    TResult? Function()? authorizationSuccess,
    TResult? Function()? authorizationError,
  }) {
    return needAuthorize?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? alreadyLogged,
    TResult Function()? needAuthorize,
    TResult Function()? authorizationSuccess,
    TResult Function()? authorizationError,
    required TResult orElse(),
  }) {
    if (needAuthorize != null) {
      return needAuthorize();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(AlreadyLogged value) alreadyLogged,
    required TResult Function(NeedAuthorize value) needAuthorize,
    required TResult Function(AuthorizationSuccess value) authorizationSuccess,
    required TResult Function(AuthorizationError value) authorizationError,
  }) {
    return needAuthorize(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(AlreadyLogged value)? alreadyLogged,
    TResult? Function(NeedAuthorize value)? needAuthorize,
    TResult? Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult? Function(AuthorizationError value)? authorizationError,
  }) {
    return needAuthorize?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(AlreadyLogged value)? alreadyLogged,
    TResult Function(NeedAuthorize value)? needAuthorize,
    TResult Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult Function(AuthorizationError value)? authorizationError,
    required TResult orElse(),
  }) {
    if (needAuthorize != null) {
      return needAuthorize(this);
    }
    return orElse();
  }
}

abstract class NeedAuthorize implements AuthState {
  const factory NeedAuthorize() = _$NeedAuthorize;
}

/// @nodoc
abstract class _$$AuthorizationSuccessCopyWith<$Res> {
  factory _$$AuthorizationSuccessCopyWith(_$AuthorizationSuccess value,
          $Res Function(_$AuthorizationSuccess) then) =
      __$$AuthorizationSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthorizationSuccessCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res, _$AuthorizationSuccess>
    implements _$$AuthorizationSuccessCopyWith<$Res> {
  __$$AuthorizationSuccessCopyWithImpl(_$AuthorizationSuccess _value,
      $Res Function(_$AuthorizationSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AuthorizationSuccess implements AuthorizationSuccess {
  const _$AuthorizationSuccess();

  @override
  String toString() {
    return 'AuthState.authorizationSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AuthorizationSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() alreadyLogged,
    required TResult Function() needAuthorize,
    required TResult Function() authorizationSuccess,
    required TResult Function() authorizationError,
  }) {
    return authorizationSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? alreadyLogged,
    TResult? Function()? needAuthorize,
    TResult? Function()? authorizationSuccess,
    TResult? Function()? authorizationError,
  }) {
    return authorizationSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? alreadyLogged,
    TResult Function()? needAuthorize,
    TResult Function()? authorizationSuccess,
    TResult Function()? authorizationError,
    required TResult orElse(),
  }) {
    if (authorizationSuccess != null) {
      return authorizationSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(AlreadyLogged value) alreadyLogged,
    required TResult Function(NeedAuthorize value) needAuthorize,
    required TResult Function(AuthorizationSuccess value) authorizationSuccess,
    required TResult Function(AuthorizationError value) authorizationError,
  }) {
    return authorizationSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(AlreadyLogged value)? alreadyLogged,
    TResult? Function(NeedAuthorize value)? needAuthorize,
    TResult? Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult? Function(AuthorizationError value)? authorizationError,
  }) {
    return authorizationSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(AlreadyLogged value)? alreadyLogged,
    TResult Function(NeedAuthorize value)? needAuthorize,
    TResult Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult Function(AuthorizationError value)? authorizationError,
    required TResult orElse(),
  }) {
    if (authorizationSuccess != null) {
      return authorizationSuccess(this);
    }
    return orElse();
  }
}

abstract class AuthorizationSuccess implements AuthState {
  const factory AuthorizationSuccess() = _$AuthorizationSuccess;
}

/// @nodoc
abstract class _$$AuthorizationErrorCopyWith<$Res> {
  factory _$$AuthorizationErrorCopyWith(_$AuthorizationError value,
          $Res Function(_$AuthorizationError) then) =
      __$$AuthorizationErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthorizationErrorCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res, _$AuthorizationError>
    implements _$$AuthorizationErrorCopyWith<$Res> {
  __$$AuthorizationErrorCopyWithImpl(
      _$AuthorizationError _value, $Res Function(_$AuthorizationError) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AuthorizationError implements AuthorizationError {
  const _$AuthorizationError();

  @override
  String toString() {
    return 'AuthState.authorizationError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AuthorizationError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() alreadyLogged,
    required TResult Function() needAuthorize,
    required TResult Function() authorizationSuccess,
    required TResult Function() authorizationError,
  }) {
    return authorizationError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? alreadyLogged,
    TResult? Function()? needAuthorize,
    TResult? Function()? authorizationSuccess,
    TResult? Function()? authorizationError,
  }) {
    return authorizationError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? alreadyLogged,
    TResult Function()? needAuthorize,
    TResult Function()? authorizationSuccess,
    TResult Function()? authorizationError,
    required TResult orElse(),
  }) {
    if (authorizationError != null) {
      return authorizationError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(AlreadyLogged value) alreadyLogged,
    required TResult Function(NeedAuthorize value) needAuthorize,
    required TResult Function(AuthorizationSuccess value) authorizationSuccess,
    required TResult Function(AuthorizationError value) authorizationError,
  }) {
    return authorizationError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(AlreadyLogged value)? alreadyLogged,
    TResult? Function(NeedAuthorize value)? needAuthorize,
    TResult? Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult? Function(AuthorizationError value)? authorizationError,
  }) {
    return authorizationError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(AlreadyLogged value)? alreadyLogged,
    TResult Function(NeedAuthorize value)? needAuthorize,
    TResult Function(AuthorizationSuccess value)? authorizationSuccess,
    TResult Function(AuthorizationError value)? authorizationError,
    required TResult orElse(),
  }) {
    if (authorizationError != null) {
      return authorizationError(this);
    }
    return orElse();
  }
}

abstract class AuthorizationError implements AuthState {
  const factory AuthorizationError() = _$AuthorizationError;
}

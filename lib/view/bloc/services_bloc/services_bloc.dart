import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:keepcode_test/constants/app_constants.dart';
import 'package:keepcode_test/domain/repositories/services_repository.dart';

import '../../../domain/models/service_model.dart';
import '../../../domain/models/translates_data_model.dart';
import '../../../get_it.dart' as get_it;

part 'services_bloc.freezed.dart';
part 'services_event.dart';
part 'services_state.dart';

class ServicesBloc extends Bloc<ServicesEvent, ServicesState> {
  final ServicesRepository repository;
  ServicesBloc(this.repository) : super(const Loading()) {
    final hiveBox = get_it.sl<Box>();
    on<FetchAllServices>((event, emit) async {
      final result = await repository.fetchServices();
      result.fold((l) {
        emit(const ErrorServicesFetching());
      }, (r) {
        final List<ServiceModel> servicesList = serviceModelFromJson(r);
        final ServicesTranslatesList translatesList = hiveBox.get(AppConstants.servicesTranslatesList);
        final List<ServiceInfoReadyToShow> readyList = [];
        for (var e in servicesList) {
          final String shortName = e.shortName ?? '';
          translatesList.listOfItems.forEach((element) {
            if (element.shortName == shortName) {
              readyList.add(ServiceInfoReadyToShow(
                name: element.name,
                price: e.minPrice,
                count: e.totalCount,
                shortName: shortName,
              ));
            }
          });
        }
        // hiveBox.put(AppConstants.servicesReadyList, readyList);
        emit(SuccessServicesFetching(readyList));
      });
    });

    on<BuyNumber>((event, emit) async {
      emit(const Loading());
      final result = await repository.buyNumber(serviceId: event.serviceId);
      result.fold((l) {
        emit(const ErrorBuyingNumber());
      }, (r) {
        emit(const SuccessBuyingNumber());
      });
    });
  }
}

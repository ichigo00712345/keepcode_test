part of 'services_bloc.dart';

@freezed
class ServicesEvent with _$ServicesEvent {
  const factory ServicesEvent.fetchAllServices() = FetchAllServices;
  const factory ServicesEvent.buyNumber({required String serviceId}) = BuyNumber;
}

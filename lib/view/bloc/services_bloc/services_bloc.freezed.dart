// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'services_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ServicesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchAllServices,
    required TResult Function(String serviceId) buyNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchAllServices,
    TResult? Function(String serviceId)? buyNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchAllServices,
    TResult Function(String serviceId)? buyNumber,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchAllServices value) fetchAllServices,
    required TResult Function(BuyNumber value) buyNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchAllServices value)? fetchAllServices,
    TResult? Function(BuyNumber value)? buyNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchAllServices value)? fetchAllServices,
    TResult Function(BuyNumber value)? buyNumber,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServicesEventCopyWith<$Res> {
  factory $ServicesEventCopyWith(
          ServicesEvent value, $Res Function(ServicesEvent) then) =
      _$ServicesEventCopyWithImpl<$Res, ServicesEvent>;
}

/// @nodoc
class _$ServicesEventCopyWithImpl<$Res, $Val extends ServicesEvent>
    implements $ServicesEventCopyWith<$Res> {
  _$ServicesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$FetchAllServicesCopyWith<$Res> {
  factory _$$FetchAllServicesCopyWith(
          _$FetchAllServices value, $Res Function(_$FetchAllServices) then) =
      __$$FetchAllServicesCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FetchAllServicesCopyWithImpl<$Res>
    extends _$ServicesEventCopyWithImpl<$Res, _$FetchAllServices>
    implements _$$FetchAllServicesCopyWith<$Res> {
  __$$FetchAllServicesCopyWithImpl(
      _$FetchAllServices _value, $Res Function(_$FetchAllServices) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FetchAllServices implements FetchAllServices {
  const _$FetchAllServices();

  @override
  String toString() {
    return 'ServicesEvent.fetchAllServices()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FetchAllServices);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchAllServices,
    required TResult Function(String serviceId) buyNumber,
  }) {
    return fetchAllServices();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchAllServices,
    TResult? Function(String serviceId)? buyNumber,
  }) {
    return fetchAllServices?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchAllServices,
    TResult Function(String serviceId)? buyNumber,
    required TResult orElse(),
  }) {
    if (fetchAllServices != null) {
      return fetchAllServices();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchAllServices value) fetchAllServices,
    required TResult Function(BuyNumber value) buyNumber,
  }) {
    return fetchAllServices(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchAllServices value)? fetchAllServices,
    TResult? Function(BuyNumber value)? buyNumber,
  }) {
    return fetchAllServices?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchAllServices value)? fetchAllServices,
    TResult Function(BuyNumber value)? buyNumber,
    required TResult orElse(),
  }) {
    if (fetchAllServices != null) {
      return fetchAllServices(this);
    }
    return orElse();
  }
}

abstract class FetchAllServices implements ServicesEvent {
  const factory FetchAllServices() = _$FetchAllServices;
}

/// @nodoc
abstract class _$$BuyNumberCopyWith<$Res> {
  factory _$$BuyNumberCopyWith(
          _$BuyNumber value, $Res Function(_$BuyNumber) then) =
      __$$BuyNumberCopyWithImpl<$Res>;
  @useResult
  $Res call({String serviceId});
}

/// @nodoc
class __$$BuyNumberCopyWithImpl<$Res>
    extends _$ServicesEventCopyWithImpl<$Res, _$BuyNumber>
    implements _$$BuyNumberCopyWith<$Res> {
  __$$BuyNumberCopyWithImpl(
      _$BuyNumber _value, $Res Function(_$BuyNumber) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? serviceId = null,
  }) {
    return _then(_$BuyNumber(
      serviceId: null == serviceId
          ? _value.serviceId
          : serviceId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$BuyNumber implements BuyNumber {
  const _$BuyNumber({required this.serviceId});

  @override
  final String serviceId;

  @override
  String toString() {
    return 'ServicesEvent.buyNumber(serviceId: $serviceId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BuyNumber &&
            (identical(other.serviceId, serviceId) ||
                other.serviceId == serviceId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, serviceId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BuyNumberCopyWith<_$BuyNumber> get copyWith =>
      __$$BuyNumberCopyWithImpl<_$BuyNumber>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchAllServices,
    required TResult Function(String serviceId) buyNumber,
  }) {
    return buyNumber(serviceId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchAllServices,
    TResult? Function(String serviceId)? buyNumber,
  }) {
    return buyNumber?.call(serviceId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchAllServices,
    TResult Function(String serviceId)? buyNumber,
    required TResult orElse(),
  }) {
    if (buyNumber != null) {
      return buyNumber(serviceId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchAllServices value) fetchAllServices,
    required TResult Function(BuyNumber value) buyNumber,
  }) {
    return buyNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchAllServices value)? fetchAllServices,
    TResult? Function(BuyNumber value)? buyNumber,
  }) {
    return buyNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchAllServices value)? fetchAllServices,
    TResult Function(BuyNumber value)? buyNumber,
    required TResult orElse(),
  }) {
    if (buyNumber != null) {
      return buyNumber(this);
    }
    return orElse();
  }
}

abstract class BuyNumber implements ServicesEvent {
  const factory BuyNumber({required final String serviceId}) = _$BuyNumber;

  String get serviceId;
  @JsonKey(ignore: true)
  _$$BuyNumberCopyWith<_$BuyNumber> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ServicesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<ServiceInfoReadyToShow> itemsList)
        successServicesFetching,
    required TResult Function() errorServicesFetching,
    required TResult Function() successBuyingNumber,
    required TResult Function() errorBuyingNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult? Function()? errorServicesFetching,
    TResult? Function()? successBuyingNumber,
    TResult? Function()? errorBuyingNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult Function()? errorServicesFetching,
    TResult Function()? successBuyingNumber,
    TResult Function()? errorBuyingNumber,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessServicesFetching value)
        successServicesFetching,
    required TResult Function(ErrorServicesFetching value)
        errorServicesFetching,
    required TResult Function(SuccessBuyingNumber value) successBuyingNumber,
    required TResult Function(ErrorBuyingNumber value) errorBuyingNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessServicesFetching value)? successServicesFetching,
    TResult? Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult? Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult? Function(ErrorBuyingNumber value)? errorBuyingNumber,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessServicesFetching value)? successServicesFetching,
    TResult Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult Function(ErrorBuyingNumber value)? errorBuyingNumber,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServicesStateCopyWith<$Res> {
  factory $ServicesStateCopyWith(
          ServicesState value, $Res Function(ServicesState) then) =
      _$ServicesStateCopyWithImpl<$Res, ServicesState>;
}

/// @nodoc
class _$ServicesStateCopyWithImpl<$Res, $Val extends ServicesState>
    implements $ServicesStateCopyWith<$Res> {
  _$ServicesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadingCopyWith<$Res> {
  factory _$$LoadingCopyWith(_$Loading value, $Res Function(_$Loading) then) =
      __$$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingCopyWithImpl<$Res>
    extends _$ServicesStateCopyWithImpl<$Res, _$Loading>
    implements _$$LoadingCopyWith<$Res> {
  __$$LoadingCopyWithImpl(_$Loading _value, $Res Function(_$Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Loading implements Loading {
  const _$Loading();

  @override
  String toString() {
    return 'ServicesState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<ServiceInfoReadyToShow> itemsList)
        successServicesFetching,
    required TResult Function() errorServicesFetching,
    required TResult Function() successBuyingNumber,
    required TResult Function() errorBuyingNumber,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult? Function()? errorServicesFetching,
    TResult? Function()? successBuyingNumber,
    TResult? Function()? errorBuyingNumber,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult Function()? errorServicesFetching,
    TResult Function()? successBuyingNumber,
    TResult Function()? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessServicesFetching value)
        successServicesFetching,
    required TResult Function(ErrorServicesFetching value)
        errorServicesFetching,
    required TResult Function(SuccessBuyingNumber value) successBuyingNumber,
    required TResult Function(ErrorBuyingNumber value) errorBuyingNumber,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessServicesFetching value)? successServicesFetching,
    TResult? Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult? Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult? Function(ErrorBuyingNumber value)? errorBuyingNumber,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessServicesFetching value)? successServicesFetching,
    TResult Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult Function(ErrorBuyingNumber value)? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading implements ServicesState {
  const factory Loading() = _$Loading;
}

/// @nodoc
abstract class _$$SuccessServicesFetchingCopyWith<$Res> {
  factory _$$SuccessServicesFetchingCopyWith(_$SuccessServicesFetching value,
          $Res Function(_$SuccessServicesFetching) then) =
      __$$SuccessServicesFetchingCopyWithImpl<$Res>;
  @useResult
  $Res call({List<ServiceInfoReadyToShow> itemsList});
}

/// @nodoc
class __$$SuccessServicesFetchingCopyWithImpl<$Res>
    extends _$ServicesStateCopyWithImpl<$Res, _$SuccessServicesFetching>
    implements _$$SuccessServicesFetchingCopyWith<$Res> {
  __$$SuccessServicesFetchingCopyWithImpl(_$SuccessServicesFetching _value,
      $Res Function(_$SuccessServicesFetching) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? itemsList = null,
  }) {
    return _then(_$SuccessServicesFetching(
      null == itemsList
          ? _value._itemsList
          : itemsList // ignore: cast_nullable_to_non_nullable
              as List<ServiceInfoReadyToShow>,
    ));
  }
}

/// @nodoc

class _$SuccessServicesFetching implements SuccessServicesFetching {
  const _$SuccessServicesFetching(final List<ServiceInfoReadyToShow> itemsList)
      : _itemsList = itemsList;

  final List<ServiceInfoReadyToShow> _itemsList;
  @override
  List<ServiceInfoReadyToShow> get itemsList {
    if (_itemsList is EqualUnmodifiableListView) return _itemsList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_itemsList);
  }

  @override
  String toString() {
    return 'ServicesState.successServicesFetching(itemsList: $itemsList)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SuccessServicesFetching &&
            const DeepCollectionEquality()
                .equals(other._itemsList, _itemsList));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_itemsList));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SuccessServicesFetchingCopyWith<_$SuccessServicesFetching> get copyWith =>
      __$$SuccessServicesFetchingCopyWithImpl<_$SuccessServicesFetching>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<ServiceInfoReadyToShow> itemsList)
        successServicesFetching,
    required TResult Function() errorServicesFetching,
    required TResult Function() successBuyingNumber,
    required TResult Function() errorBuyingNumber,
  }) {
    return successServicesFetching(itemsList);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult? Function()? errorServicesFetching,
    TResult? Function()? successBuyingNumber,
    TResult? Function()? errorBuyingNumber,
  }) {
    return successServicesFetching?.call(itemsList);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult Function()? errorServicesFetching,
    TResult Function()? successBuyingNumber,
    TResult Function()? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (successServicesFetching != null) {
      return successServicesFetching(itemsList);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessServicesFetching value)
        successServicesFetching,
    required TResult Function(ErrorServicesFetching value)
        errorServicesFetching,
    required TResult Function(SuccessBuyingNumber value) successBuyingNumber,
    required TResult Function(ErrorBuyingNumber value) errorBuyingNumber,
  }) {
    return successServicesFetching(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessServicesFetching value)? successServicesFetching,
    TResult? Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult? Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult? Function(ErrorBuyingNumber value)? errorBuyingNumber,
  }) {
    return successServicesFetching?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessServicesFetching value)? successServicesFetching,
    TResult Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult Function(ErrorBuyingNumber value)? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (successServicesFetching != null) {
      return successServicesFetching(this);
    }
    return orElse();
  }
}

abstract class SuccessServicesFetching implements ServicesState {
  const factory SuccessServicesFetching(
      final List<ServiceInfoReadyToShow> itemsList) = _$SuccessServicesFetching;

  List<ServiceInfoReadyToShow> get itemsList;
  @JsonKey(ignore: true)
  _$$SuccessServicesFetchingCopyWith<_$SuccessServicesFetching> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ErrorServicesFetchingCopyWith<$Res> {
  factory _$$ErrorServicesFetchingCopyWith(_$ErrorServicesFetching value,
          $Res Function(_$ErrorServicesFetching) then) =
      __$$ErrorServicesFetchingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ErrorServicesFetchingCopyWithImpl<$Res>
    extends _$ServicesStateCopyWithImpl<$Res, _$ErrorServicesFetching>
    implements _$$ErrorServicesFetchingCopyWith<$Res> {
  __$$ErrorServicesFetchingCopyWithImpl(_$ErrorServicesFetching _value,
      $Res Function(_$ErrorServicesFetching) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ErrorServicesFetching implements ErrorServicesFetching {
  const _$ErrorServicesFetching();

  @override
  String toString() {
    return 'ServicesState.errorServicesFetching()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ErrorServicesFetching);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<ServiceInfoReadyToShow> itemsList)
        successServicesFetching,
    required TResult Function() errorServicesFetching,
    required TResult Function() successBuyingNumber,
    required TResult Function() errorBuyingNumber,
  }) {
    return errorServicesFetching();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult? Function()? errorServicesFetching,
    TResult? Function()? successBuyingNumber,
    TResult? Function()? errorBuyingNumber,
  }) {
    return errorServicesFetching?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult Function()? errorServicesFetching,
    TResult Function()? successBuyingNumber,
    TResult Function()? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (errorServicesFetching != null) {
      return errorServicesFetching();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessServicesFetching value)
        successServicesFetching,
    required TResult Function(ErrorServicesFetching value)
        errorServicesFetching,
    required TResult Function(SuccessBuyingNumber value) successBuyingNumber,
    required TResult Function(ErrorBuyingNumber value) errorBuyingNumber,
  }) {
    return errorServicesFetching(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessServicesFetching value)? successServicesFetching,
    TResult? Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult? Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult? Function(ErrorBuyingNumber value)? errorBuyingNumber,
  }) {
    return errorServicesFetching?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessServicesFetching value)? successServicesFetching,
    TResult Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult Function(ErrorBuyingNumber value)? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (errorServicesFetching != null) {
      return errorServicesFetching(this);
    }
    return orElse();
  }
}

abstract class ErrorServicesFetching implements ServicesState {
  const factory ErrorServicesFetching() = _$ErrorServicesFetching;
}

/// @nodoc
abstract class _$$SuccessBuyingNumberCopyWith<$Res> {
  factory _$$SuccessBuyingNumberCopyWith(_$SuccessBuyingNumber value,
          $Res Function(_$SuccessBuyingNumber) then) =
      __$$SuccessBuyingNumberCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SuccessBuyingNumberCopyWithImpl<$Res>
    extends _$ServicesStateCopyWithImpl<$Res, _$SuccessBuyingNumber>
    implements _$$SuccessBuyingNumberCopyWith<$Res> {
  __$$SuccessBuyingNumberCopyWithImpl(
      _$SuccessBuyingNumber _value, $Res Function(_$SuccessBuyingNumber) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SuccessBuyingNumber implements SuccessBuyingNumber {
  const _$SuccessBuyingNumber();

  @override
  String toString() {
    return 'ServicesState.successBuyingNumber()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SuccessBuyingNumber);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<ServiceInfoReadyToShow> itemsList)
        successServicesFetching,
    required TResult Function() errorServicesFetching,
    required TResult Function() successBuyingNumber,
    required TResult Function() errorBuyingNumber,
  }) {
    return successBuyingNumber();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult? Function()? errorServicesFetching,
    TResult? Function()? successBuyingNumber,
    TResult? Function()? errorBuyingNumber,
  }) {
    return successBuyingNumber?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult Function()? errorServicesFetching,
    TResult Function()? successBuyingNumber,
    TResult Function()? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (successBuyingNumber != null) {
      return successBuyingNumber();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessServicesFetching value)
        successServicesFetching,
    required TResult Function(ErrorServicesFetching value)
        errorServicesFetching,
    required TResult Function(SuccessBuyingNumber value) successBuyingNumber,
    required TResult Function(ErrorBuyingNumber value) errorBuyingNumber,
  }) {
    return successBuyingNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessServicesFetching value)? successServicesFetching,
    TResult? Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult? Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult? Function(ErrorBuyingNumber value)? errorBuyingNumber,
  }) {
    return successBuyingNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessServicesFetching value)? successServicesFetching,
    TResult Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult Function(ErrorBuyingNumber value)? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (successBuyingNumber != null) {
      return successBuyingNumber(this);
    }
    return orElse();
  }
}

abstract class SuccessBuyingNumber implements ServicesState {
  const factory SuccessBuyingNumber() = _$SuccessBuyingNumber;
}

/// @nodoc
abstract class _$$ErrorBuyingNumberCopyWith<$Res> {
  factory _$$ErrorBuyingNumberCopyWith(
          _$ErrorBuyingNumber value, $Res Function(_$ErrorBuyingNumber) then) =
      __$$ErrorBuyingNumberCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ErrorBuyingNumberCopyWithImpl<$Res>
    extends _$ServicesStateCopyWithImpl<$Res, _$ErrorBuyingNumber>
    implements _$$ErrorBuyingNumberCopyWith<$Res> {
  __$$ErrorBuyingNumberCopyWithImpl(
      _$ErrorBuyingNumber _value, $Res Function(_$ErrorBuyingNumber) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ErrorBuyingNumber implements ErrorBuyingNumber {
  const _$ErrorBuyingNumber();

  @override
  String toString() {
    return 'ServicesState.errorBuyingNumber()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ErrorBuyingNumber);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<ServiceInfoReadyToShow> itemsList)
        successServicesFetching,
    required TResult Function() errorServicesFetching,
    required TResult Function() successBuyingNumber,
    required TResult Function() errorBuyingNumber,
  }) {
    return errorBuyingNumber();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult? Function()? errorServicesFetching,
    TResult? Function()? successBuyingNumber,
    TResult? Function()? errorBuyingNumber,
  }) {
    return errorBuyingNumber?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<ServiceInfoReadyToShow> itemsList)?
        successServicesFetching,
    TResult Function()? errorServicesFetching,
    TResult Function()? successBuyingNumber,
    TResult Function()? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (errorBuyingNumber != null) {
      return errorBuyingNumber();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessServicesFetching value)
        successServicesFetching,
    required TResult Function(ErrorServicesFetching value)
        errorServicesFetching,
    required TResult Function(SuccessBuyingNumber value) successBuyingNumber,
    required TResult Function(ErrorBuyingNumber value) errorBuyingNumber,
  }) {
    return errorBuyingNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessServicesFetching value)? successServicesFetching,
    TResult? Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult? Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult? Function(ErrorBuyingNumber value)? errorBuyingNumber,
  }) {
    return errorBuyingNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessServicesFetching value)? successServicesFetching,
    TResult Function(ErrorServicesFetching value)? errorServicesFetching,
    TResult Function(SuccessBuyingNumber value)? successBuyingNumber,
    TResult Function(ErrorBuyingNumber value)? errorBuyingNumber,
    required TResult orElse(),
  }) {
    if (errorBuyingNumber != null) {
      return errorBuyingNumber(this);
    }
    return orElse();
  }
}

abstract class ErrorBuyingNumber implements ServicesState {
  const factory ErrorBuyingNumber() = _$ErrorBuyingNumber;
}

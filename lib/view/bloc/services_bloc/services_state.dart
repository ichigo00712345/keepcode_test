part of 'services_bloc.dart';

@freezed
class ServicesState with _$ServicesState {
  const factory ServicesState.loading() = Loading;
  const factory ServicesState.successServicesFetching(List<ServiceInfoReadyToShow> itemsList) = SuccessServicesFetching;
  const factory ServicesState.errorServicesFetching() = ErrorServicesFetching;
  const factory ServicesState.successBuyingNumber() = SuccessBuyingNumber;
  const factory ServicesState.errorBuyingNumber() = ErrorBuyingNumber;
}

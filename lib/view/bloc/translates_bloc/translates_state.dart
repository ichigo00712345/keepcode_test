part of 'translates_bloc.dart';

@freezed
class TranslatesState with _$TranslatesState {
  const factory TranslatesState.loading() = Loading;
  const factory TranslatesState.success() = SuccessTranslationFetch;
  const factory TranslatesState.error() = ErrorTranslationFetch;
}

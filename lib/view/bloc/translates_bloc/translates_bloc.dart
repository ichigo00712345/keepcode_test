import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:keepcode_test/domain/repositories/translates_repository.dart';

import '../../../constants/app_constants.dart';
import '../../../domain/models/translates_data_model.dart';
import '../../../get_it.dart' as get_it;

part 'translates_bloc.freezed.dart';
part 'translates_event.dart';
part 'translates_state.dart';

class TranslatesBloc extends Bloc<TranslatesEvent, TranslatesState> {
  final TranslatesRepository repository;
  TranslatesBloc(this.repository) : super(const Loading()) {
    final hiveBox = get_it.sl<Box>();
    on<FetchTranslationData>((event, emit) async {
      emit(const Loading());
      final result = await repository.fetchTranslates();
      result.fold((l) {
        emit(const ErrorTranslationFetch());
      }, (r) {
        final servicesList = ServicesTranslatesList.fromJSON(r);
        hiveBox.put(AppConstants.servicesTranslatesList, servicesList);
        emit(const SuccessTranslationFetch());
      });
    });
  }
}

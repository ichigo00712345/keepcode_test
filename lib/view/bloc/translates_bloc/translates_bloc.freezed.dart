// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'translates_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TranslatesEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchTranslationData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchTranslationData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchTranslationData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchTranslationData value) fetchTranslationData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchTranslationData value)? fetchTranslationData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchTranslationData value)? fetchTranslationData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TranslatesEventCopyWith<$Res> {
  factory $TranslatesEventCopyWith(
          TranslatesEvent value, $Res Function(TranslatesEvent) then) =
      _$TranslatesEventCopyWithImpl<$Res, TranslatesEvent>;
}

/// @nodoc
class _$TranslatesEventCopyWithImpl<$Res, $Val extends TranslatesEvent>
    implements $TranslatesEventCopyWith<$Res> {
  _$TranslatesEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$FetchTranslationDataCopyWith<$Res> {
  factory _$$FetchTranslationDataCopyWith(_$FetchTranslationData value,
          $Res Function(_$FetchTranslationData) then) =
      __$$FetchTranslationDataCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FetchTranslationDataCopyWithImpl<$Res>
    extends _$TranslatesEventCopyWithImpl<$Res, _$FetchTranslationData>
    implements _$$FetchTranslationDataCopyWith<$Res> {
  __$$FetchTranslationDataCopyWithImpl(_$FetchTranslationData _value,
      $Res Function(_$FetchTranslationData) _then)
      : super(_value, _then);
}

/// @nodoc

class _$FetchTranslationData implements FetchTranslationData {
  const _$FetchTranslationData();

  @override
  String toString() {
    return 'TranslatesEvent.fetchTranslationData()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FetchTranslationData);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetchTranslationData,
  }) {
    return fetchTranslationData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? fetchTranslationData,
  }) {
    return fetchTranslationData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetchTranslationData,
    required TResult orElse(),
  }) {
    if (fetchTranslationData != null) {
      return fetchTranslationData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchTranslationData value) fetchTranslationData,
  }) {
    return fetchTranslationData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(FetchTranslationData value)? fetchTranslationData,
  }) {
    return fetchTranslationData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchTranslationData value)? fetchTranslationData,
    required TResult orElse(),
  }) {
    if (fetchTranslationData != null) {
      return fetchTranslationData(this);
    }
    return orElse();
  }
}

abstract class FetchTranslationData implements TranslatesEvent {
  const factory FetchTranslationData() = _$FetchTranslationData;
}

/// @nodoc
mixin _$TranslatesState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessTranslationFetch value) success,
    required TResult Function(ErrorTranslationFetch value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessTranslationFetch value)? success,
    TResult? Function(ErrorTranslationFetch value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessTranslationFetch value)? success,
    TResult Function(ErrorTranslationFetch value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TranslatesStateCopyWith<$Res> {
  factory $TranslatesStateCopyWith(
          TranslatesState value, $Res Function(TranslatesState) then) =
      _$TranslatesStateCopyWithImpl<$Res, TranslatesState>;
}

/// @nodoc
class _$TranslatesStateCopyWithImpl<$Res, $Val extends TranslatesState>
    implements $TranslatesStateCopyWith<$Res> {
  _$TranslatesStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadingCopyWith<$Res> {
  factory _$$LoadingCopyWith(_$Loading value, $Res Function(_$Loading) then) =
      __$$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingCopyWithImpl<$Res>
    extends _$TranslatesStateCopyWithImpl<$Res, _$Loading>
    implements _$$LoadingCopyWith<$Res> {
  __$$LoadingCopyWithImpl(_$Loading _value, $Res Function(_$Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Loading implements Loading {
  const _$Loading();

  @override
  String toString() {
    return 'TranslatesState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function() error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function()? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessTranslationFetch value) success,
    required TResult Function(ErrorTranslationFetch value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessTranslationFetch value)? success,
    TResult? Function(ErrorTranslationFetch value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessTranslationFetch value)? success,
    TResult Function(ErrorTranslationFetch value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading implements TranslatesState {
  const factory Loading() = _$Loading;
}

/// @nodoc
abstract class _$$SuccessTranslationFetchCopyWith<$Res> {
  factory _$$SuccessTranslationFetchCopyWith(_$SuccessTranslationFetch value,
          $Res Function(_$SuccessTranslationFetch) then) =
      __$$SuccessTranslationFetchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SuccessTranslationFetchCopyWithImpl<$Res>
    extends _$TranslatesStateCopyWithImpl<$Res, _$SuccessTranslationFetch>
    implements _$$SuccessTranslationFetchCopyWith<$Res> {
  __$$SuccessTranslationFetchCopyWithImpl(_$SuccessTranslationFetch _value,
      $Res Function(_$SuccessTranslationFetch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SuccessTranslationFetch implements SuccessTranslationFetch {
  const _$SuccessTranslationFetch();

  @override
  String toString() {
    return 'TranslatesState.success()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SuccessTranslationFetch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function() error,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function()? error,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessTranslationFetch value) success,
    required TResult Function(ErrorTranslationFetch value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessTranslationFetch value)? success,
    TResult? Function(ErrorTranslationFetch value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessTranslationFetch value)? success,
    TResult Function(ErrorTranslationFetch value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SuccessTranslationFetch implements TranslatesState {
  const factory SuccessTranslationFetch() = _$SuccessTranslationFetch;
}

/// @nodoc
abstract class _$$ErrorTranslationFetchCopyWith<$Res> {
  factory _$$ErrorTranslationFetchCopyWith(_$ErrorTranslationFetch value,
          $Res Function(_$ErrorTranslationFetch) then) =
      __$$ErrorTranslationFetchCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ErrorTranslationFetchCopyWithImpl<$Res>
    extends _$TranslatesStateCopyWithImpl<$Res, _$ErrorTranslationFetch>
    implements _$$ErrorTranslationFetchCopyWith<$Res> {
  __$$ErrorTranslationFetchCopyWithImpl(_$ErrorTranslationFetch _value,
      $Res Function(_$ErrorTranslationFetch) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ErrorTranslationFetch implements ErrorTranslationFetch {
  const _$ErrorTranslationFetch();

  @override
  String toString() {
    return 'TranslatesState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ErrorTranslationFetch);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() success,
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? success,
    TResult? Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? success,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Loading value) loading,
    required TResult Function(SuccessTranslationFetch value) success,
    required TResult Function(ErrorTranslationFetch value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Loading value)? loading,
    TResult? Function(SuccessTranslationFetch value)? success,
    TResult? Function(ErrorTranslationFetch value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Loading value)? loading,
    TResult Function(SuccessTranslationFetch value)? success,
    TResult Function(ErrorTranslationFetch value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ErrorTranslationFetch implements TranslatesState {
  const factory ErrorTranslationFetch() = _$ErrorTranslationFetch;
}

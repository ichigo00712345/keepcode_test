part of 'translates_bloc.dart';

@freezed
class TranslatesEvent with _$TranslatesEvent {
  const factory TranslatesEvent.fetchTranslationData() = FetchTranslationData;
}

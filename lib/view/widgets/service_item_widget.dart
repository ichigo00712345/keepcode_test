import 'package:flutter/material.dart';

class ServiceItemWidget extends StatelessWidget {
  final String name;
  final int count;
  final double price;
  final String shortName;
  final VoidCallback onPressed;
  const ServiceItemWidget(
      {super.key,
      required this.name,
      required this.count,
      required this.price,
      required this.shortName,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: Theme.of(context).colorScheme.primary,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.4,
              child: Text(
                name,
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                // overflow: TextOverflow.ellipsis,
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Price: $price\$'),
                  Text('Count: $count'),
                ],
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            ElevatedButton(onPressed: onPressed, child: const Text('Buy'))
          ],
        ),
      ),
    );
  }
}

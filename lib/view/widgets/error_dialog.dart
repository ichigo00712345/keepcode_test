import 'package:flutter/material.dart';

showErrorDialog(BuildContext context, String text,
    {Function? onPress, Widget? animation, String? buttonText, bool? enablePopScope = true}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return WillPopScope(
        onWillPop: enablePopScope == true ? () async => false : () async => true,
        child: AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
          ),
          content: Text(
            text,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w600,
            ),
          ),
          actionsAlignment: MainAxisAlignment.center,
          actions: <Widget>[
            ElevatedButton(
              onPressed: () {
                if (Navigator.of(context).canPop()) {
                  Navigator.of(context).pop();
                }
                if (onPress != null) {
                  onPress();
                }
              },
              child: Text(buttonText ?? 'Try again'),
            ),
          ],
        ),
      );
    },
  );
}

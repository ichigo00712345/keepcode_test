import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepcode_test/common/utils.dart';
import 'package:keepcode_test/domain/repositories/auth_repository.dart';
import 'package:keepcode_test/view/widgets/error_dialog.dart';

import '../../../get_it.dart' as get_it;
import '../bloc/auth_bloc/auth_bloc.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBloc(get_it.sl.get<AuthRepository>())..add(const CheckAuthorizationStatus()),
      child: BlocConsumer<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is AuthorizationSuccess) {
            navigateTo(context, '/translates');
          } else if (state is AuthorizationError) {
            showErrorDialog(context, 'Something went wrong! :(\nPlease try again!', onPress: () {
              context.read<AuthBloc>().add(const Authorization());
            });
          } else if (state is AlreadyLogged) {
            navigateTo(context, '/translates');
          } else if (state is NeedAuthorize) {
            context.read<AuthBloc>().add(const Authorization());
          }
        },
        builder: (context, state) {
          return Scaffold(
            backgroundColor: const Color(0xff333333),
            body: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/splash.png',
                    width: MediaQuery.of(context).size.width * 0.8,
                  ),
                  const SizedBox(height: 20),
                  if (state is Loading) const CircularProgressIndicator()
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

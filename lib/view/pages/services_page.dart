import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepcode_test/domain/repositories/services_repository.dart';

import '../../../get_it.dart' as get_it;
import '../bloc/services_bloc/services_bloc.dart';
import '../widgets/service_item_widget.dart';

class ServicesPage extends StatelessWidget {
  const ServicesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ServicesBloc(get_it.sl<ServicesRepository>())..add(const FetchAllServices()),
      child: BlocConsumer<ServicesBloc, ServicesState>(
        listener: (context, state) {},
        builder: (context, state) {
          return state is SuccessServicesFetching
              ? ListView.builder(
                  itemBuilder: (context, index) {
                    final item = state.itemsList[index];
                    return ServiceItemWidget(
                      onPressed: () {
                        context.read<ServicesBloc>().add(BuyNumber(serviceId: item.shortName ?? ''));
                      },
                      name: item.name ?? 'Unnamed',
                      count: item.count ?? 0,
                      price: item.price ?? 0.0,
                      shortName: item.shortName ?? '',
                    );
                  },
                  itemCount: state.itemsList.length,
                )
              : const Center(
                  child: CircularProgressIndicator(),
                );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:keepcode_test/view/pages/services_page.dart';

import 'my_numbers_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late String title;
  late int selectedIndex;
  late PageController pageController;
  late List<Widget> pages;

  @override
  void initState() {
    pages = [const ServicesPage(), const MyNumbersPage()];
    selectedIndex = 0;
    title = "All Services";
    pageController = PageController(initialPage: selectedIndex);
    super.initState();
  }

  changePage(index) {
    if (selectedIndex != index) {
      selectedIndex = index;
      title = selectedIndex == 0 ? "All Services" : 'My numbers';
      setState(() {});
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: "All services",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.phone),
            label: "My numbers",
          ),
        ],
        selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
        unselectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600),
        currentIndex: selectedIndex,
        onTap: changePage,
        fixedColor: Theme.of(context).colorScheme.primary,
      ),
// SuccessServicesFetching
      body: PageView.builder(
        physics: const NeverScrollableScrollPhysics(),
        controller: pageController,
        itemCount: pages.length,
        itemBuilder: (BuildContext context, int index) {
          return pages.elementAt(selectedIndex);
          // return state is StatusReceived ? pages.elementAt(_selectedIndex) : state is Loading ? const Center(child: Loader()) ;
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:keepcode_test/view/pages/translates_page.dart';

import 'auth_page.dart';
import 'home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        '/auth': (context) => const AuthPage(),
        '/translates': (context) => const TranslatesPage(),
        '/home': (context) => const HomePage(),
      },
      theme: ThemeData(
        fontFamily: 'Mont',
        useMaterial3: true,
        colorSchemeSeed: const Color(0xffea5400),
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        // colorSchemeSeed: const Color(0xffea5400),
        colorSchemeSeed: const Color(0xff333333),
        useMaterial3: true,
        brightness: Brightness.dark,
      ),
      home: const AuthPage(),
      // home: const TranslatesPage(),
      // home: const ServicesPage(),
      // home: const HomePage(),
    );
  }
}

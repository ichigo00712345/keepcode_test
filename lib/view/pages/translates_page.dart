import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keepcode_test/common/utils.dart';

import '../../../get_it.dart' as get_it;
import '../../domain/repositories/translates_repository.dart';
import '../bloc/translates_bloc/translates_bloc.dart';
import '../widgets/error_dialog.dart';

class TranslatesPage extends StatelessWidget {
  const TranslatesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TranslatesBloc(get_it.sl.get<TranslatesRepository>())..add(const FetchTranslationData()),
      child: BlocConsumer<TranslatesBloc, TranslatesState>(
        listener: (context, state) {
          if (state is SuccessTranslationFetch) {
            navigateTo(context, '/home');
          } else if (state is ErrorTranslationFetch) {
            showErrorDialog(context, 'Something went wrong! :(\nPlease try again!', onPress: () {
              context.read<TranslatesBloc>().add(const FetchTranslationData());
            });
          }
        },
        builder: (context, state) {
          return Scaffold(
            body: state is Loading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : const SizedBox(),
          );
        },
      ),
    );
  }
}

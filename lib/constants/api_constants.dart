class ApiBase {
  static const host = "https://sms-activate.org/stubs";
}

class ApiCustomer {
  static const host = ApiBase.host;

  static const translates = "/apiMobile.php?owner=6&action=getAllServicesAndAllCountries"; // get
  static const auth = "/handler_auth.php";
  static const allServices = "/apiMobile.php?owner=6&action=getAllServices"; // get
  static const buyNumber =
      "/handler_api.php?refresh_token=57732d7a05fb0fda93e4783c97788a21&sessionId=5210a808d88da491087fcb9ec792291c&owner=6&userid=1155497&country=0&service=передаем сюда id выбранного сервиса&action=getNumberMobile&forward=0&operator=any"; // get
  static const removeActivation =
      "/handler_api.php?refresh_token=57732d7a05fb0fda93e4783c97788a21&sessionId=5210a808d88da491087fcb9ec792291c&owner=6&userid=1155497&action=setStatus&id=1350100086&status=8";
}

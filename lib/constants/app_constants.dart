abstract class AppConstants {
  static const authTime = 'LAST_AUTH_TIME';
  static const authInfo = 'AUTH_INFO';
  static const servicesTranslatesList = "SERVICES_TRANSLATES_LIST";
  static const servicesReadyList = "SERVICES_READY_LIST";
}

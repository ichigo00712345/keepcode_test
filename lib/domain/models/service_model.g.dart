// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'service_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ServiceModelAdapter extends TypeAdapter<ServiceModel> {
  @override
  final int typeId = 3;

  @override
  ServiceModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ServiceModel();
  }

  @override
  void write(BinaryWriter writer, ServiceModel obj) {
    writer.writeByte(0);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ServiceModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ServiceModel _$$_ServiceModelFromJson(Map<String, dynamic> json) =>
    _$_ServiceModel(
      shortName: json['shortName'] as String?,
      sellTop: json['sellTop'] as int?,
      forward: json['forward'] as bool?,
      totalCount: json['totalCount'] as int?,
      minPrice: (json['minPrice'] as num?)?.toDouble(),
      minFreePrice: (json['minFreePrice'] as num?)?.toDouble(),
      countWithFreePrice: json['countWithFreePrice'] as int?,
      onlyRent: json['onlyRent'] as bool?,
    );

Map<String, dynamic> _$$_ServiceModelToJson(_$_ServiceModel instance) =>
    <String, dynamic>{
      'shortName': instance.shortName,
      'sellTop': instance.sellTop,
      'forward': instance.forward,
      'totalCount': instance.totalCount,
      'minPrice': instance.minPrice,
      'minFreePrice': instance.minFreePrice,
      'countWithFreePrice': instance.countWithFreePrice,
      'onlyRent': instance.onlyRent,
    };

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_data_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AuthDataModel _$AuthDataModelFromJson(Map<String, dynamic> json) {
  return _AuthDataModel.fromJson(json);
}

/// @nodoc
mixin _$AuthDataModel {
  @HiveField(0)
  String? get sessionId => throw _privateConstructorUsedError;
  @HiveField(1)
  String? get balance => throw _privateConstructorUsedError;
  @HiveField(2)
  String? get cashback => throw _privateConstructorUsedError;
  @HiveField(3)
  String? get refreshToken => throw _privateConstructorUsedError;
  @HiveField(4)
  int? get userid => throw _privateConstructorUsedError;
  @HiveField(5)
  String? get email => throw _privateConstructorUsedError;
  @HiveField(6)
  String? get telegramId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AuthDataModelCopyWith<AuthDataModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthDataModelCopyWith<$Res> {
  factory $AuthDataModelCopyWith(
          AuthDataModel value, $Res Function(AuthDataModel) then) =
      _$AuthDataModelCopyWithImpl<$Res, AuthDataModel>;
  @useResult
  $Res call(
      {@HiveField(0) String? sessionId,
      @HiveField(1) String? balance,
      @HiveField(2) String? cashback,
      @HiveField(3) String? refreshToken,
      @HiveField(4) int? userid,
      @HiveField(5) String? email,
      @HiveField(6) String? telegramId});
}

/// @nodoc
class _$AuthDataModelCopyWithImpl<$Res, $Val extends AuthDataModel>
    implements $AuthDataModelCopyWith<$Res> {
  _$AuthDataModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sessionId = freezed,
    Object? balance = freezed,
    Object? cashback = freezed,
    Object? refreshToken = freezed,
    Object? userid = freezed,
    Object? email = freezed,
    Object? telegramId = freezed,
  }) {
    return _then(_value.copyWith(
      sessionId: freezed == sessionId
          ? _value.sessionId
          : sessionId // ignore: cast_nullable_to_non_nullable
              as String?,
      balance: freezed == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as String?,
      cashback: freezed == cashback
          ? _value.cashback
          : cashback // ignore: cast_nullable_to_non_nullable
              as String?,
      refreshToken: freezed == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String?,
      userid: freezed == userid
          ? _value.userid
          : userid // ignore: cast_nullable_to_non_nullable
              as int?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      telegramId: freezed == telegramId
          ? _value.telegramId
          : telegramId // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_AuthDataModelCopyWith<$Res>
    implements $AuthDataModelCopyWith<$Res> {
  factory _$$_AuthDataModelCopyWith(
          _$_AuthDataModel value, $Res Function(_$_AuthDataModel) then) =
      __$$_AuthDataModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) String? sessionId,
      @HiveField(1) String? balance,
      @HiveField(2) String? cashback,
      @HiveField(3) String? refreshToken,
      @HiveField(4) int? userid,
      @HiveField(5) String? email,
      @HiveField(6) String? telegramId});
}

/// @nodoc
class __$$_AuthDataModelCopyWithImpl<$Res>
    extends _$AuthDataModelCopyWithImpl<$Res, _$_AuthDataModel>
    implements _$$_AuthDataModelCopyWith<$Res> {
  __$$_AuthDataModelCopyWithImpl(
      _$_AuthDataModel _value, $Res Function(_$_AuthDataModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sessionId = freezed,
    Object? balance = freezed,
    Object? cashback = freezed,
    Object? refreshToken = freezed,
    Object? userid = freezed,
    Object? email = freezed,
    Object? telegramId = freezed,
  }) {
    return _then(_$_AuthDataModel(
      sessionId: freezed == sessionId
          ? _value.sessionId
          : sessionId // ignore: cast_nullable_to_non_nullable
              as String?,
      balance: freezed == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as String?,
      cashback: freezed == cashback
          ? _value.cashback
          : cashback // ignore: cast_nullable_to_non_nullable
              as String?,
      refreshToken: freezed == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String?,
      userid: freezed == userid
          ? _value.userid
          : userid // ignore: cast_nullable_to_non_nullable
              as int?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      telegramId: freezed == telegramId
          ? _value.telegramId
          : telegramId // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AuthDataModel implements _AuthDataModel {
  const _$_AuthDataModel(
      {@HiveField(0) this.sessionId,
      @HiveField(1) this.balance,
      @HiveField(2) this.cashback,
      @HiveField(3) this.refreshToken,
      @HiveField(4) this.userid,
      @HiveField(5) this.email,
      @HiveField(6) this.telegramId});

  factory _$_AuthDataModel.fromJson(Map<String, dynamic> json) =>
      _$$_AuthDataModelFromJson(json);

  @override
  @HiveField(0)
  final String? sessionId;
  @override
  @HiveField(1)
  final String? balance;
  @override
  @HiveField(2)
  final String? cashback;
  @override
  @HiveField(3)
  final String? refreshToken;
  @override
  @HiveField(4)
  final int? userid;
  @override
  @HiveField(5)
  final String? email;
  @override
  @HiveField(6)
  final String? telegramId;

  @override
  String toString() {
    return 'AuthDataModel(sessionId: $sessionId, balance: $balance, cashback: $cashback, refreshToken: $refreshToken, userid: $userid, email: $email, telegramId: $telegramId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AuthDataModel &&
            (identical(other.sessionId, sessionId) ||
                other.sessionId == sessionId) &&
            (identical(other.balance, balance) || other.balance == balance) &&
            (identical(other.cashback, cashback) ||
                other.cashback == cashback) &&
            (identical(other.refreshToken, refreshToken) ||
                other.refreshToken == refreshToken) &&
            (identical(other.userid, userid) || other.userid == userid) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.telegramId, telegramId) ||
                other.telegramId == telegramId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, sessionId, balance, cashback,
      refreshToken, userid, email, telegramId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AuthDataModelCopyWith<_$_AuthDataModel> get copyWith =>
      __$$_AuthDataModelCopyWithImpl<_$_AuthDataModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AuthDataModelToJson(
      this,
    );
  }
}

abstract class _AuthDataModel implements AuthDataModel {
  const factory _AuthDataModel(
      {@HiveField(0) final String? sessionId,
      @HiveField(1) final String? balance,
      @HiveField(2) final String? cashback,
      @HiveField(3) final String? refreshToken,
      @HiveField(4) final int? userid,
      @HiveField(5) final String? email,
      @HiveField(6) final String? telegramId}) = _$_AuthDataModel;

  factory _AuthDataModel.fromJson(Map<String, dynamic> json) =
      _$_AuthDataModel.fromJson;

  @override
  @HiveField(0)
  String? get sessionId;
  @override
  @HiveField(1)
  String? get balance;
  @override
  @HiveField(2)
  String? get cashback;
  @override
  @HiveField(3)
  String? get refreshToken;
  @override
  @HiveField(4)
  int? get userid;
  @override
  @HiveField(5)
  String? get email;
  @override
  @HiveField(6)
  String? get telegramId;
  @override
  @JsonKey(ignore: true)
  _$$_AuthDataModelCopyWith<_$_AuthDataModel> get copyWith =>
      throw _privateConstructorUsedError;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'translates_data_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ServicesTranslatesListAdapter
    extends TypeAdapter<ServicesTranslatesList> {
  @override
  final int typeId = 1;

  @override
  ServicesTranslatesList read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ServicesTranslatesList(
      listOfItems: (fields[0] as List).cast<ServicesTranslatesItem>(),
    );
  }

  @override
  void write(BinaryWriter writer, ServicesTranslatesList obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.listOfItems);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ServicesTranslatesListAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class ServicesTranslatesItemAdapter
    extends TypeAdapter<ServicesTranslatesItem> {
  @override
  final int typeId = 2;

  @override
  ServicesTranslatesItem read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return ServicesTranslatesItem(
      shortName: fields[0] as String?,
      name: fields[1] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, ServicesTranslatesItem obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.shortName)
      ..writeByte(1)
      ..write(obj.name);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ServicesTranslatesItemAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

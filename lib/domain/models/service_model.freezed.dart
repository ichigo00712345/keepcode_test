// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'service_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ServiceModel _$ServiceModelFromJson(Map<String, dynamic> json) {
  return _ServiceModel.fromJson(json);
}

/// @nodoc
mixin _$ServiceModel {
  @HiveField(0)
  String? get shortName => throw _privateConstructorUsedError;
  @HiveField(1)
  int? get sellTop => throw _privateConstructorUsedError;
  @HiveField(2)
  bool? get forward => throw _privateConstructorUsedError;
  @HiveField(3)
  int? get totalCount => throw _privateConstructorUsedError;
  @HiveField(4)
  double? get minPrice => throw _privateConstructorUsedError;
  @HiveField(5)
  double? get minFreePrice => throw _privateConstructorUsedError;
  @HiveField(6)
  int? get countWithFreePrice => throw _privateConstructorUsedError;
  @HiveField(7)
  bool? get onlyRent => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ServiceModelCopyWith<ServiceModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServiceModelCopyWith<$Res> {
  factory $ServiceModelCopyWith(
          ServiceModel value, $Res Function(ServiceModel) then) =
      _$ServiceModelCopyWithImpl<$Res, ServiceModel>;
  @useResult
  $Res call(
      {@HiveField(0) String? shortName,
      @HiveField(1) int? sellTop,
      @HiveField(2) bool? forward,
      @HiveField(3) int? totalCount,
      @HiveField(4) double? minPrice,
      @HiveField(5) double? minFreePrice,
      @HiveField(6) int? countWithFreePrice,
      @HiveField(7) bool? onlyRent});
}

/// @nodoc
class _$ServiceModelCopyWithImpl<$Res, $Val extends ServiceModel>
    implements $ServiceModelCopyWith<$Res> {
  _$ServiceModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shortName = freezed,
    Object? sellTop = freezed,
    Object? forward = freezed,
    Object? totalCount = freezed,
    Object? minPrice = freezed,
    Object? minFreePrice = freezed,
    Object? countWithFreePrice = freezed,
    Object? onlyRent = freezed,
  }) {
    return _then(_value.copyWith(
      shortName: freezed == shortName
          ? _value.shortName
          : shortName // ignore: cast_nullable_to_non_nullable
              as String?,
      sellTop: freezed == sellTop
          ? _value.sellTop
          : sellTop // ignore: cast_nullable_to_non_nullable
              as int?,
      forward: freezed == forward
          ? _value.forward
          : forward // ignore: cast_nullable_to_non_nullable
              as bool?,
      totalCount: freezed == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int?,
      minPrice: freezed == minPrice
          ? _value.minPrice
          : minPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      minFreePrice: freezed == minFreePrice
          ? _value.minFreePrice
          : minFreePrice // ignore: cast_nullable_to_non_nullable
              as double?,
      countWithFreePrice: freezed == countWithFreePrice
          ? _value.countWithFreePrice
          : countWithFreePrice // ignore: cast_nullable_to_non_nullable
              as int?,
      onlyRent: freezed == onlyRent
          ? _value.onlyRent
          : onlyRent // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ServiceModelCopyWith<$Res>
    implements $ServiceModelCopyWith<$Res> {
  factory _$$_ServiceModelCopyWith(
          _$_ServiceModel value, $Res Function(_$_ServiceModel) then) =
      __$$_ServiceModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) String? shortName,
      @HiveField(1) int? sellTop,
      @HiveField(2) bool? forward,
      @HiveField(3) int? totalCount,
      @HiveField(4) double? minPrice,
      @HiveField(5) double? minFreePrice,
      @HiveField(6) int? countWithFreePrice,
      @HiveField(7) bool? onlyRent});
}

/// @nodoc
class __$$_ServiceModelCopyWithImpl<$Res>
    extends _$ServiceModelCopyWithImpl<$Res, _$_ServiceModel>
    implements _$$_ServiceModelCopyWith<$Res> {
  __$$_ServiceModelCopyWithImpl(
      _$_ServiceModel _value, $Res Function(_$_ServiceModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shortName = freezed,
    Object? sellTop = freezed,
    Object? forward = freezed,
    Object? totalCount = freezed,
    Object? minPrice = freezed,
    Object? minFreePrice = freezed,
    Object? countWithFreePrice = freezed,
    Object? onlyRent = freezed,
  }) {
    return _then(_$_ServiceModel(
      shortName: freezed == shortName
          ? _value.shortName
          : shortName // ignore: cast_nullable_to_non_nullable
              as String?,
      sellTop: freezed == sellTop
          ? _value.sellTop
          : sellTop // ignore: cast_nullable_to_non_nullable
              as int?,
      forward: freezed == forward
          ? _value.forward
          : forward // ignore: cast_nullable_to_non_nullable
              as bool?,
      totalCount: freezed == totalCount
          ? _value.totalCount
          : totalCount // ignore: cast_nullable_to_non_nullable
              as int?,
      minPrice: freezed == minPrice
          ? _value.minPrice
          : minPrice // ignore: cast_nullable_to_non_nullable
              as double?,
      minFreePrice: freezed == minFreePrice
          ? _value.minFreePrice
          : minFreePrice // ignore: cast_nullable_to_non_nullable
              as double?,
      countWithFreePrice: freezed == countWithFreePrice
          ? _value.countWithFreePrice
          : countWithFreePrice // ignore: cast_nullable_to_non_nullable
              as int?,
      onlyRent: freezed == onlyRent
          ? _value.onlyRent
          : onlyRent // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ServiceModel implements _ServiceModel {
  const _$_ServiceModel(
      {@HiveField(0) this.shortName,
      @HiveField(1) this.sellTop,
      @HiveField(2) this.forward,
      @HiveField(3) this.totalCount,
      @HiveField(4) this.minPrice,
      @HiveField(5) this.minFreePrice,
      @HiveField(6) this.countWithFreePrice,
      @HiveField(7) this.onlyRent});

  factory _$_ServiceModel.fromJson(Map<String, dynamic> json) =>
      _$$_ServiceModelFromJson(json);

  @override
  @HiveField(0)
  final String? shortName;
  @override
  @HiveField(1)
  final int? sellTop;
  @override
  @HiveField(2)
  final bool? forward;
  @override
  @HiveField(3)
  final int? totalCount;
  @override
  @HiveField(4)
  final double? minPrice;
  @override
  @HiveField(5)
  final double? minFreePrice;
  @override
  @HiveField(6)
  final int? countWithFreePrice;
  @override
  @HiveField(7)
  final bool? onlyRent;

  @override
  String toString() {
    return 'ServiceModel(shortName: $shortName, sellTop: $sellTop, forward: $forward, totalCount: $totalCount, minPrice: $minPrice, minFreePrice: $minFreePrice, countWithFreePrice: $countWithFreePrice, onlyRent: $onlyRent)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ServiceModel &&
            (identical(other.shortName, shortName) ||
                other.shortName == shortName) &&
            (identical(other.sellTop, sellTop) || other.sellTop == sellTop) &&
            (identical(other.forward, forward) || other.forward == forward) &&
            (identical(other.totalCount, totalCount) ||
                other.totalCount == totalCount) &&
            (identical(other.minPrice, minPrice) ||
                other.minPrice == minPrice) &&
            (identical(other.minFreePrice, minFreePrice) ||
                other.minFreePrice == minFreePrice) &&
            (identical(other.countWithFreePrice, countWithFreePrice) ||
                other.countWithFreePrice == countWithFreePrice) &&
            (identical(other.onlyRent, onlyRent) ||
                other.onlyRent == onlyRent));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, shortName, sellTop, forward,
      totalCount, minPrice, minFreePrice, countWithFreePrice, onlyRent);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ServiceModelCopyWith<_$_ServiceModel> get copyWith =>
      __$$_ServiceModelCopyWithImpl<_$_ServiceModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ServiceModelToJson(
      this,
    );
  }
}

abstract class _ServiceModel implements ServiceModel {
  const factory _ServiceModel(
      {@HiveField(0) final String? shortName,
      @HiveField(1) final int? sellTop,
      @HiveField(2) final bool? forward,
      @HiveField(3) final int? totalCount,
      @HiveField(4) final double? minPrice,
      @HiveField(5) final double? minFreePrice,
      @HiveField(6) final int? countWithFreePrice,
      @HiveField(7) final bool? onlyRent}) = _$_ServiceModel;

  factory _ServiceModel.fromJson(Map<String, dynamic> json) =
      _$_ServiceModel.fromJson;

  @override
  @HiveField(0)
  String? get shortName;
  @override
  @HiveField(1)
  int? get sellTop;
  @override
  @HiveField(2)
  bool? get forward;
  @override
  @HiveField(3)
  int? get totalCount;
  @override
  @HiveField(4)
  double? get minPrice;
  @override
  @HiveField(5)
  double? get minFreePrice;
  @override
  @HiveField(6)
  int? get countWithFreePrice;
  @override
  @HiveField(7)
  bool? get onlyRent;
  @override
  @JsonKey(ignore: true)
  _$$_ServiceModelCopyWith<_$_ServiceModel> get copyWith =>
      throw _privateConstructorUsedError;
}

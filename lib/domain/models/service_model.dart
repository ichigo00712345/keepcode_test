// To parse this JSON data, do
//
//     final serviceModel = serviceModelFromJson(jsonString);

import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'service_model.freezed.dart';
part 'service_model.g.dart';

List<ServiceModel> serviceModelFromJson(String str) =>
    List<ServiceModel>.from(json.decode(str).map((x) => ServiceModel.fromJson(x)));

String serviceModelToJson(List<ServiceModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@freezed
@HiveType(typeId: 3)
class ServiceModel with _$ServiceModel {
  const factory ServiceModel({
    @HiveField(0) String? shortName,
    @HiveField(1) int? sellTop,
    @HiveField(2) bool? forward,
    @HiveField(3) int? totalCount,
    @HiveField(4) double? minPrice,
    @HiveField(5) double? minFreePrice,
    @HiveField(6) int? countWithFreePrice,
    @HiveField(7) bool? onlyRent,
  }) = _ServiceModel;

  factory ServiceModel.fromJson(Map<String, dynamic> json) => _$ServiceModelFromJson(json);
}

class ServiceInfoReadyToShow {
  String? name;
  int? count;
  double? price;
  String? shortName;
  ServiceInfoReadyToShow({
    this.name,
    this.count,
    this.price,
    this.shortName,
  });
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_data_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AuthDataModelAdapter extends TypeAdapter<AuthDataModel> {
  @override
  final int typeId = 0;

  @override
  AuthDataModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AuthDataModel();
  }

  @override
  void write(BinaryWriter writer, AuthDataModel obj) {
    writer.writeByte(0);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AuthDataModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AuthDataModel _$$_AuthDataModelFromJson(Map<String, dynamic> json) =>
    _$_AuthDataModel(
      sessionId: json['sessionId'] as String?,
      balance: json['balance'] as String?,
      cashback: json['cashback'] as String?,
      refreshToken: json['refreshToken'] as String?,
      userid: json['userid'] as int?,
      email: json['email'] as String?,
      telegramId: json['telegramId'] as String?,
    );

Map<String, dynamic> _$$_AuthDataModelToJson(_$_AuthDataModel instance) =>
    <String, dynamic>{
      'sessionId': instance.sessionId,
      'balance': instance.balance,
      'cashback': instance.cashback,
      'refreshToken': instance.refreshToken,
      'userid': instance.userid,
      'email': instance.email,
      'telegramId': instance.telegramId,
    };

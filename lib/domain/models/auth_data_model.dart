import 'dart:convert';

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'auth_data_model.freezed.dart';
part 'auth_data_model.g.dart';

AuthDataModel authDataModelFromJson(String str) => AuthDataModel.fromJson(json.decode(str));

String authDataModelToJson(AuthDataModel data) => json.encode(data.toJson());

@freezed
@HiveType(typeId: 0)
class AuthDataModel with _$AuthDataModel {
  const factory AuthDataModel({
    @HiveField(0) String? sessionId,
    @HiveField(1) String? balance,
    @HiveField(2) String? cashback,
    @HiveField(3) String? refreshToken,
    @HiveField(4) int? userid,
    @HiveField(5) String? email,
    @HiveField(6) String? telegramId,
  }) = _AuthDataModel;

  factory AuthDataModel.fromJson(Map<String, dynamic> json) => _$AuthDataModelFromJson(json);
}

import 'dart:convert';

import 'package:hive_flutter/hive_flutter.dart';

part 'translates_data_model.g.dart';

@HiveType(typeId: 1)
class ServicesTranslatesList {
  @HiveField(0)
  final List<ServicesTranslatesItem> listOfItems;
  ServicesTranslatesList({required this.listOfItems});

  factory ServicesTranslatesList.fromJSON(String json) {
    final Map<String, dynamic> servicesData = jsonDecode(json)["services"];
    final List<ServicesTranslatesItem> listOfItems = [];
    servicesData.forEach((key, value) {
      listOfItems.add(
        ServicesTranslatesItem(
          name: value["en"],
          shortName: key.substring(0, 2),
        ),
      );
    });
    return ServicesTranslatesList(listOfItems: listOfItems);
  }
}

@HiveType(typeId: 2)
class ServicesTranslatesItem {
  @HiveField(0)
  final String? shortName;
  @HiveField(1)
  final String? name;
  ServicesTranslatesItem({
    this.shortName,
    this.name,
  });
}

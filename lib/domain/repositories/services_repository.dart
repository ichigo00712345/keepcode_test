import 'package:dartz/dartz.dart';

import '../../common/failure.dart';

abstract class ServicesRepository {
  Future<Either<Failure, String>> fetchServices();
  Future<Either<Failure, String>> buyNumber({required String serviceId});
}

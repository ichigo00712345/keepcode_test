import 'package:dartz/dartz.dart';

import '../../common/failure.dart';

abstract class TranslatesRepository {
  Future<Either<Failure, dynamic>> fetchTranslates();
}

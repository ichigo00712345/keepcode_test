import 'package:dartz/dartz.dart';

import '../../common/failure.dart';

abstract class AuthRepository {
  Future<Either<Failure, String>> authorization();
}

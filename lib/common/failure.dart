import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final String message;
  final int status;

  const Failure(this.message, this.status);
  @override
  List<Object?> get props => [message, status];
}

class ResponseFailure extends Failure {
  const ResponseFailure({String message = 'responce error', int status = 0}) : super(message, status);

  factory ResponseFailure.fromResponse(response) {
    if (response is Response) {
      int? stCode = response.statusCode;

      if (stCode is int && stCode >= 500) {
      } else if (response.data is Map<String, dynamic> && response.data['detail'] != null) {
        return ResponseFailure(message: response.data['detail'].toString(), status: response.statusCode ?? 0);
      }
      return ResponseFailure(message: response.data.toString(), status: response.statusCode ?? 0);
    }

    return ResponseFailure(message: response.toString(), status: 599);
  }
  @override
  List<Object?> get props => [message, status];
}

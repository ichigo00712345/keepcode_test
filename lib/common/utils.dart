import 'package:flutter/material.dart';

navigateTo(BuildContext context, String routeName, {Map<String, dynamic>? arguments}) {
  Navigator.pushNamedAndRemoveUntil(context, routeName, (Route<dynamic> route) => false, arguments: arguments);
}

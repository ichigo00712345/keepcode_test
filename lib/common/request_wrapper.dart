import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import 'failure.dart';

class RequestWrapper<T> {
  Future<Either<Failure, T>> request(Function() request) async {
    try {
      // запуск нужного запроса
      var result = await request();
      return Right(result);
    } on DioError catch (e) {
      // обработка исключений
      return Left(ResponseFailure.fromResponse(e.response ?? e));
    } catch (e) {
      return Left(ResponseFailure.fromResponse(e));
    }
  }
}
